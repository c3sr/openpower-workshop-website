package main

import (
	"net/http"
	"os"
)

const (
	DEFAULT_PORT = "8080"
)

func main() {
	port, found := os.LookupEnv("PORT")
	if !found {
		port = DEFAULT_PORT
	}
	http.Handle("/", http.FileServer(http.Dir("static")))
	// ip, _ := GetLocalIp()
	// serveLoc := ip + ":" + port
	// log.Printf("http://" + serveLoc)
	http.ListenAndServe(":"+port, nil)
}
